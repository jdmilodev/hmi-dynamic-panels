# Beckhoff-HMI-Demo---Arrays-and-Structures
One method of using UserControls with arrays and structures

## Versions

* TwinCAT 3.1.4022.16
* TwinCAT HMI 1.8.861.0

## About

This is a demonstration for a binding only solution to working with arrays and structures when drilling down through multiple panels.

The PLC must be running locally for the HMI to function correctly.

* Each Boiler may have up to 4 Circuits
* Each Ciruit may have up to 4 Pumps
* Each Pump has 1 Pump Motor

This is not an optimal solution to this problem.  The use of Javascript would enable the components to be fully dynamic but at this stage this is not included in the scope of the project.

## Credits

<div>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
